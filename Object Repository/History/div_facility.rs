<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_facility</name>
   <tag></tag>
   <elementGuidId>036773fe-359c-45b9-9c8f-7e2d087627c6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.panel-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='history']/div/div[2]/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8a437033-4f3d-4d42-ad03-3237438047e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>panel-body</value>
      <webElementGuid>2eb3f507-1b9c-4757-a631-9e525dcbd5b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            Facility
                        
                        
                            Tokyo CURA Healthcare Center
                        
                        

                        
                            Apply for hospital readmission
                        
                        
                            Yes
                        
                        

                        
                            Healthcare Program
                        
                        
                            Medicare
                        
                        

                        
                            Comment
                        
                        
                            Test comment
                        
                    </value>
      <webElementGuid>d66a5f73-d5e9-4f3d-91fb-a936c55056b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;history&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-offset-2 col-sm-8&quot;]/div[@class=&quot;panel panel-info&quot;]/div[@class=&quot;panel-body&quot;]</value>
      <webElementGuid>b52413b6-3f0d-48c9-b4e2-4c039b968037</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='history']/div/div[2]/div/div/div[2]</value>
      <webElementGuid>a8b3817f-ea6d-4fa1-808b-c225689025f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='History'])[2]/following::div[5]</value>
      <webElementGuid>0b18fe21-5776-4edb-a982-9df58c5b6bef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::div[8]</value>
      <webElementGuid>b3325ef1-b45c-4a6b-b510-831598634a4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[2]</value>
      <webElementGuid>462690e7-2fc4-4674-84f1-3a2896ae5e38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            Facility
                        
                        
                            Tokyo CURA Healthcare Center
                        
                        

                        
                            Apply for hospital readmission
                        
                        
                            Yes
                        
                        

                        
                            Healthcare Program
                        
                        
                            Medicare
                        
                        

                        
                            Comment
                        
                        
                            Test comment
                        
                    ' or . = '
                        
                            Facility
                        
                        
                            Tokyo CURA Healthcare Center
                        
                        

                        
                            Apply for hospital readmission
                        
                        
                            Yes
                        
                        

                        
                            Healthcare Program
                        
                        
                            Medicare
                        
                        

                        
                            Comment
                        
                        
                            Test comment
                        
                    ')]</value>
      <webElementGuid>c8372f2e-cf88-4674-906c-85f0055f552b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
